Definición de una sola máquina virtual con Vagrant

El modo más básico de definir un entorno Vagrant, es la definición de un entorno
de máquina única. Este tipo de entorno define una única máquina virtual que se 
administra con un simple comando de Vagrant. 

Que utilizaremos:
  1- Archivo Vagrantfile
  2- Una box de debian/buster64

Pasos: 
  1- Crear carpeta de trabajo
      Cree una carpeta de trabajo:
        $ mkdir ej1_vm1
        $ cd ej1_vm1
  2- Crear archivo Vagrantfile
        $ vagrant init --minimal debian/buster64
  3- Configurar y hacer ajustes al arhcivo Vagrantfile
      Para este caso los parámetros minimos ya estan por defecto
  4- Iniciar entorno
        $ vagrant up
  5- Comprobar estado de la maquina
        $ vagrant status
  6- Login en la máquina virtual
        $ vagrant ssh
  7- Apagado de la máquina virtual
        $ vagrant halt
  8- Destruir la máquina (si fuere necesario)
        $ vagrant destroy
