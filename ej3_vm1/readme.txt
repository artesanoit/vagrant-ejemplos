Definición de una máquina virtual con Vagrant y configurar una carpeta compartida

En este ejemplo utilizaremos un entonro vagrant sencillo donde se compartirá un 
directorio entre el sistema operativo invitado y el sistema operativo anfitrion. 

Hay una serie de escenario donde es muy útil compartir una carpeta entre el sistema 
operativo host y el sistema operativo invidato con Vagrant. Unos de los usos 
principales de Vagrant es para desarrollar software, bajo este escenario las 
carpetas compartidas pueden convertirse rápidamente en la principal ventaja de usar 
Vagrant. Una carpeta compartida nos permite ejecutar código dentro de un entorno de 
servidor y al mismo tiempo nos permite tener acceso al todas las herramientas de 
desarrollo disponibles en nuestro sistema operativo anfitrion.

Que utilizaremos:
  1- Archivo Vagrantfile
  2- Una box de debian/buster64

Pasos: 
  1- Crear carpeta de trabajo
      Cree una carpeta de trabajo
        $ mkdir ej3_vm1
        $ cd ej3_vm1
  2- Crear archivo Vagrantfile
        $ vagrant init --minimal debian/buster64
  3- Configurar y hacer ajustes al arhcivo Vagrantfile
        Para lograr nuestro objetivo utilizaremos:
          config.vm.synced_folder "./datos_vm/", "/vagrant"
  4- Iniciar entorno
        $ vagrant up
  5- Comprobar estado de la maquina
        $ vagrant status
  6- Login en la máquina virtual
        $ vagrant ssh
  7- Probar la carpeta compartida 
        Copie o cree algunos archivos de texto y
        utilice rsync para sicroinizar la camperta en el sistema anfitrion
  8- Apagado de la máquina virtual
        $ vagrant halt
  9- Destruir la máquina (opcional)
        $ vagrant destroy
