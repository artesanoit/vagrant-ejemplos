# ej5_vm1

## Aprovisionamiento Básico: 
### Ejecución de comandos en el entorno virtual!

Para entornos virtuales basados en sistemas operativos linux el aprovisionamiento consiste en ejecutar comandos básicos de shell como bash o sh (entre otros).

En este ejemplo, utilizaremos comando de shell para insertar en un archivo de texto un mensaje, con el comando `echo`:

```sh 
echo "Hola mundo desde Vagrant" > /home/vagrant/hola.txt
```
Configuracion en el `Vagrantfile`:
```ruby
config.vm.provision "shell",
    inline: "echo 'Hola mundo desde Vagrant' > /home/vagrant/hola.txt"
```


 
